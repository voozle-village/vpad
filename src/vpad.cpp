#include "vpad.h"

#include <cstring>

namespace vpad
{
  Pad::Pad()
  {
    reset();
  }

  void Pad::reset()
  {
    memset(state_, 0, sizeof(state_));
    memset(lastState_, 0, sizeof(lastState_));
  }

  void Pad::update()
  {
    memcpy(lastState_, state_, sizeof(lastState_));
  }

  bool Pad::isPressed(const int button) const
  {
    return state_[button];
  }

  bool Pad::wasPressed(const int button) const
  {
    return lastState_[button];
  }

  bool Pad::justPressed(const int button) const
  {
    return isPressed(button) && !wasPressed(button);
  }

  bool Pad::justPressedAny() const
  {
    for( int button = 0; button < MaxButtons; ++button ) {

      if( justPressed( button ) ) {

	return true;
      }
    }

    return false;
  }

  bool Pad::justReleased(const int button) const
  {
    return wasPressed(button) && !isPressed(button);
  }

  void Pad::setPressed(const int button, bool pressed)
  {
    state_[ button ] = pressed;
  }

  BufferedPad::BufferedPad()
    : queueHead_(0)
    , queueTail_(0)
  {
    memset(state_, 0, sizeof(state_));
  }

  bool BufferedPad::isPressed(const int button) const
  {
    return state_[ button ];
  }

  void BufferedPad::setPressed(const int button, bool pressed)
  {
    queue_[ queueHead_ ] = Event(button, pressed);
    queueHead_ = (queueHead_ + 1) % QueueSize;
    if( queueHead_ == queueTail_ ) {
      queueTail_ = (queueTail_ + 1) % QueueSize;
    }
  }

  BufferedPad::Event BufferedPad::nextEvent()
  {
    if( queueHead_ == queueTail_ ) {
      return Event();
    }
    else {
      const Event event = queue_[queueTail_];
      state_[ event.button ] = event.pressed;
      queueTail_ = (queueTail_ + 1) % QueueSize;
      return event;
    }
  }

  void BufferedPad::flush()
  {
    while( !nextEvent().isNull() );
  }
  
  AbstractAdapter::AbstractAdapter(AbstractPad* pad)
    : pad_(pad)
  {
  }

  AbstractAdapter::~AbstractAdapter()
  {
  }

  KeyboardAdapter::KeyboardAdapter(AbstractPad* pad)
    : AbstractAdapter(pad)
  {
    memset(keys_, 0, sizeof(keys_));
  }

  void KeyboardAdapter::onSDLEvent(SDL_Event* event)
  {
    if( event->type == SDL_KEYDOWN ) {
      const int button = getBoundButton(event->key.keysym.sym);
      if( button != -1 ) {
	getPad()->setPressed(button, true);
      }
    }
    else if( event->type == SDL_KEYUP ) {
      const int button = getBoundButton(event->key.keysym.sym);
      if( button != -1 ) {
	getPad()->setPressed(button, false);
      }
    }
  }
  
  void KeyboardAdapter::bindKey(const int button, const int key, const std::string& name)
  {
    keys_[button] = key;
    names_[button] = name;
  }

  int KeyboardAdapter::getBoundButton(const int key) const
  {
    for(int button = 0; button < MaxButtons; ++button ) {
      if(keys_[button] == key) return button;
    }
    return -1;
  }

  std::string KeyboardAdapter::getButtonName(const int button) const
  {
    return names_[button];
  }

  Repeater::Repeater(const int delay)
    : delay_(delay)
  {
    for( int button = 0; button < MaxButtons; ++button ) {
      counts_[button] = -1;
    }
  }

  void Repeater::update(AbstractPad* pad)
  {
    for( int button = 0; button < MaxButtons; ++button ) {
      if( pad->isPressed( button ) ) {
	counts_[button]++;
      }
      else {
	counts_[button] = -1;
      }
    }
  }

  bool Repeater::isPressed(const int button) const
  {
    return counts_[button] >= delay_;
  }

  void Repeater::setPressed(const int button, const bool pressed)
  {
    counts_[button] = pressed 
      ?  0
      : -1;
  }

} // ns vpad
