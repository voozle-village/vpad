#ifndef _VPAD_H_
#define _VPAD_H_

#include <SDL.h>

#include <string>

namespace vpad
{
  //! \pre
  //! - SNES -
  //!  
  //!    |        X 
  //!  --+--    Y   A
  //!    |        B  
  //!
  //! \endpre

  enum Button {

    ButtonLeft,
    ButtonRight,
    ButtonUp,
    ButtonDown,

    ButtonA,
    ButtonB,
    ButtonX,
    ButtonY,
    
    ButtonVolumeUp,
    ButtonVolumeDown,

    ButtonTriggerLeft,
    ButtonTriggerRight,
    ButtonStart,
    ButtonSelect,

    MaxButtons = 32
  };

  class AbstractPad
  {
  public:
    virtual ~AbstractPad() {}
    virtual bool isPressed(const int button) const = 0;
    virtual void setPressed(const int button, bool pressed) = 0;
  };

  class Pad : public AbstractPad
  {
  public:
    Pad();
    void reset();
    void update();
    bool isPressed(const int button) const;
    bool wasPressed(const int button) const;
    bool justPressed(const int button) const;
    bool justPressedAny() const;
    bool justReleased(const int button) const;
    void setPressed(const int button, bool pressed);
    
  private:
    bool state_[MaxButtons];
    bool lastState_[MaxButtons];
  };

  class BufferedPad : public AbstractPad
  {
  public:
    struct Event
    {
      int button;
      bool pressed;
      
      Event(const int button = -1, const bool pressed = false)
	: button(button)
	, pressed(pressed)
      {
      }

      bool isNull() const
      { 
	return button == -1;
      }
    };

    BufferedPad();
    bool isPressed(const int button) const;
    void setPressed(const int button, bool pressed);
    Event nextEvent();
    void flush();

  private:
    static const int QueueSize = 100;

    bool state_[MaxButtons];
    Event queue_[QueueSize];
    int queueHead_;
    int queueTail_;
  };

  class AbstractAdapter
  {
  public:
    AbstractAdapter(AbstractPad* pad);
    virtual ~AbstractAdapter();
    virtual void onSDLEvent(SDL_Event* event) = 0;
    
    AbstractPad* getPad()
    {
      return pad_;
    }
   
    virtual std::string getButtonName(const int button) const = 0;

  private:
    AbstractPad* pad_;
  };

  class KeyboardAdapter : public AbstractAdapter
  {
  public:
    KeyboardAdapter(AbstractPad* pad);
    void onSDLEvent(SDL_Event* event);
    void bindKey(const int button, const int key, const std::string& name);
    int getBoundButton(const int key) const;
    std::string getButtonName(const int button) const;

  private:
    int keys_[MaxButtons];
    std::string names_[MaxButtons];
  };

  class Repeater
  {
  public:
    Repeater(const int delay);
    void update(AbstractPad* pad);
    bool isPressed(const int button) const;
    void setPressed(const int button, bool pressed);

  private:
    int delay_;
    int counts_[MaxButtons];
  };

} // ns vpad

#endif /* _VPAD_H_ */
